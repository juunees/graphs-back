<?php

namespace app\controllers;
use app\models\Graph;
use app\models\Path;
use app\models\Vertex;
use Yii;
use yii\rest\ActiveController;
use yii\rest\Controller;
use yii\filters\auth\HttpBasicAuth;
use yii\web\BadRequestHttpException;
use yii\web\Response;

class GraphController extends ActiveController
{

    public $modelClass = 'app\models\Graph';

    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;
        try {
            return parent::beforeAction($action);
        } catch (BadRequestHttpException $e) {
        }
    }

    public function behaviors()
    {
        $behaviors = parent::behaviors();

        // add CORS filter
        $behaviors['corsFilter'] = [
            'class' => \yii\filters\Cors::className(),
            'cors' => [
                'Origin' => ['*'],
                'Access-Control-Request-Method' => ['GET', 'POST', 'PUT', 'PATCH', 'DELETE', 'HEAD', 'OPTIONS'],
                'Access-Control-Request-Headers' => ['*'],
            ],
        ];
        $behaviors['contentNegotiator'] = [
            'class' => \yii\filters\ContentNegotiator::className(),
            'formats' => [
                'application/json' => \yii\web\Response::FORMAT_JSON,
            ],
        ];
        return $behaviors;
    }

    public function actionCreategraph(){

        $request = Yii::$app->request;
        $name = $request->post('name');
        $graph = new Graph();
        $graph->name = $name;
        $graph->save();
        return $graph;
    }

    public function actionGetgraphs(){
        $graphs = Graph::find()->all();
        return $graphs;
    }

    public function actionGetgraph(){
        $request = Yii::$app->request;
        $id = $request->post('id');
        $graph = Graph::findOne($id);

        if($graph){
            $vertices = Vertex::find()
                ->where(['graph_id' => $graph->id])
                ->all();
            $paths = Path::find()
                ->where(['graph_id' => $graph->id])
                ->all();
            return array('graph' => $graph, 'vertices' => $vertices, 'paths' => $paths);
        }
        else
            return "graph not found";

    }

    public function actionDeletegraph(){

        $request = Yii::$app->request;
        $id = $request->post('id');
        $graph = Graph::findOne($id);

        if($graph){
            $graph->delete();
            return "ok";
        }
        else {
            return "graph not found";
        }
    }




}