<?php

namespace app\controllers;

use app\models\Graph;
use app\models\Vertex;
use Yii;
use yii\rest\ActiveController;
use yii\rest\Controller;


class VertexController extends ActiveController
{

    public $modelClass = 'app\models\Vetrex';

    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;
        try {
            return parent::beforeAction($action);
        } catch (BadRequestHttpException $e) {
        }
    }

    public function behaviors()
    {
        $behaviors = parent::behaviors();

        // add CORS filter
        $behaviors['corsFilter'] = [
            'class' => \yii\filters\Cors::className(),
            'cors' => [
                'Origin' => ['*'],
                'Access-Control-Request-Method' => ['GET', 'POST', 'PUT', 'PATCH', 'DELETE', 'HEAD', 'OPTIONS'],
                'Access-Control-Request-Headers' => ['*'],
            ],
        ];
        $behaviors['contentNegotiator'] = [
            'class' => \yii\filters\ContentNegotiator::className(),
            'formats' => [
                'application/json' => \yii\web\Response::FORMAT_JSON,
            ],
        ];
        return $behaviors;
    }

    public function actionGet(){

        $vertices = Vertex::find()->all();
        return $vertices;
    }

    public function actionCreatevertex()
    {
        $request = Yii::$app->request;
        $name = $request->post('name');
        $graphId = $request->post('graphId');

        $graph = Graph::findOne($graphId);
        if ($graph) {
            $vertex = new Vertex();
            $vertex->name = $name;
            $vertex->graph_id = $graphId;
            $vertex->save();
            return $vertex;
        } else {
            return "graph not found";
        }
    }

    public function actionDelete()
    {

        $request = Yii::$app->request;
        $id = $request->post('id');
        $vertex = Vertex::findOne($id);

        if ($vertex) {
            $vertex->delete();
            return "ok";
        } else {
            return "vertex not found";
        }
    }


}