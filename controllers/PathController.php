<?php

namespace app\controllers;
use app\models\Graph;
use app\models\Path;
use app\models\Vertex;
use SplPriorityQueue;
use SplStack;
use Yii;
use yii\helpers\Json;
use yii\rest\ActiveController;
use yii\rest\Controller;


class PathController extends ActiveController
{

    public $modelClass = 'app\models\Path';

    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;
        try {
            return parent::beforeAction($action);
        } catch (BadRequestHttpException $e) {
        }
    }

    public function behaviors()
    {
        $behaviors = parent::behaviors();

        // add CORS filter
        $behaviors['corsFilter'] = [
            'class' => \yii\filters\Cors::className(),
            'cors' => [
                'Origin' => ['*'],
                'Access-Control-Request-Method' => ['GET', 'POST', 'PUT', 'PATCH', 'DELETE', 'HEAD', 'OPTIONS'],
                'Access-Control-Request-Headers' => ['*'],
            ],
        ];
        $behaviors['contentNegotiator'] = [
            'class' => \yii\filters\ContentNegotiator::className(),
            'formats' => [
                'application/json' => \yii\web\Response::FORMAT_JSON,
            ],
        ];
        return $behaviors;
    }

    public function actionCreatepath(){

        $request = Yii::$app->request;
        $from = $request->post('from');
        $to = $request->post('to');
        $weight = $request->post('weight');
        $graphId = $request->post('graphId');
        $graph = Graph::findOne($graphId);


        if($from == $to) return "cant create path";

        $vertex1 = Vertex::findOne($from);
        $vertex2 = Vertex::findOne($to);

        if($vertex1 && $vertex2 && $graph){

            $path = Path::find()
                ->where([
                    'from'=> $from,
                    'to' => $to,
                    'graph_id' => $graphId
                ])->one();
            if(!$path) $path = new Path();

            $path->from = $from;
            $path->to = $to;
            $path->weight = $weight;
            $path->graph_id = $graphId;
            $path->save();
            return $path;
        }
        else{
            return "cant create path";
        }
    }

    public function actionDelete(){

        $request = Yii::$app->request;
        $id = $request->post('id');
        $path = Path::findOne($id);

        if($path){
            $path->delete();
            return "ok";
        }
        else {
            return "path not found";
        }
    }


    public function actionFindpath(){

       $request = Yii::$app->request;
       $from = $request->post('from');
       $to = $request->post('to');
       $graphId = $request->post('graphId');

       $paths = Path::find()
           ->where(['graph_id' => $graphId])
           ->all();

       $graph = array();

       foreach ($paths as $path){
           $graph[$path->from][$path->to] = $path->weight;
       }

       //return $graph;

//        $graph = array(
//            'A' => array('B' => 3, 'D' => 3, 'F' => 6),
//            'B' => array('A' => 2, 'D' => 1, 'E' => 3),
//            'C' => array('E' => 3, 'F' => 3),
//            'D' => array('A' => 2, 'B' => 2, 'E' => 2, 'F' => 2),
//            'E' => array('B' => 2, 'C' => 2, 'D' => 1, 'F' => 5),
//            'F' => array('A' => 7, 'C' => 4, 'D' => 4, 'E' => 6),
//        );

        return $this->shortestPath($graph,$to, $from);

    }

    public function shortestPath($graph, $source, $target) {

        $d = array();
        $pi = array();
        $Q = new SplPriorityQueue();

        foreach ($graph as $v => $adj) {
            $d[$v] = INF;
            $pi[$v] = null;
            foreach ($adj as $w => $cost) {
                $Q->insert($w, $cost);
            }
        }

        $d[$source] = 0;

        while (!$Q->isEmpty()) {
            $u = $Q->extract();
            if ( !empty($graph[$u])){
                foreach ($graph[$u] as $v => $cost) {
                    $alt = $d[$u] + $cost;
                    if ($alt < $d[$v]) {
                        $d[$v] = $alt;
                        $pi[$v] = $u;
                    }
                }
            }
        }

        $S = new SplStack();
        $u = $target;
        $dist = 0;

        while (isset($pi[$u]) && $pi[$u]) {
            $S->push($u);
            $dist += $graph[$u][$pi[$u]];
            $u = $pi[$u];
        }

        if ($S->isEmpty()) {
            return "Нет пути из ".$source." в ".$target;
        }
        else {
            return $dist;
        }
    }


}