<?php

use yii\db\Migration;

/**
 * Handles the creation of table `vertices`.
 */
class m180810_102909_create_vertices_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('vertices', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'graph_id' => $this->integer()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('vertices');
    }
}
