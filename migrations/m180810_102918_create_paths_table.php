<?php

use yii\db\Migration;

/**
 * Handles the creation of table `paths`.
 */
class m180810_102918_create_paths_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('paths', [
            'id' => $this->primaryKey(),
            'from' => $this->integer(),
            'to' => $this->integer(),
            'weight' => $this->integer(),
            'graph_id' => $this->integer()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('paths');
    }
}
